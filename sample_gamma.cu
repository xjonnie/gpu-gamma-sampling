/**
Copyright 2013 Scott Linderman (scott.linderman@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description: 
CUDA kernels to implement: 
George Marsaglia and Wai Wan Tsang. 
�A simple method for generating gamma variables.� 
ACM Trans. on Mathematical Software. Volume 26, No. 3, 
September pp. 363-372 (2000).

Known issues:
The function gamrnd() has a shared memory bug, and as a result 
you should call gamrnd_acc() followed by gamrnd_red() for 
each sample batch.

Updated:
2/19/2013
*/


#include <cuda.h>

#define ERROR_SUCCESS               0
#define ERROR_INVALID_PARAMETER     1
#define ERROR_SAMPLE_FAILURE        2
#define ERROR_BUG                   3

// Define the block size as a constant. This allows us to test 
// with different numbers of threads per block. This must be 
// a power of 2.
const int B = %(B)s;
// Define the number of threads per gamma RV
// This must divide B.
const int M = %(M)s;

/*
 * Sample a Gamma RV using the Marsaglia-Tsang algorithm. 
 * We do have some overhead from generating extra uniform
 * and normal RVs that are just rejected.
 * Our assumption is that W.H.P. we will successfully generate
 * a RV on at least one of the M threads per variable.
 * 
 * The vanilla Marsaglia alg requires alpha > 1.0
 * pU is a pointer to an array of uniform random variates, 
 * one for each thread. pN similarly points to normal
 * random variates.
 */
__global__ void gamrnd(int R,
                       float* pU,
                       float* pN,
                       float* pAlpha,
                       float* pBeta,
                       float* pG,
                       int* pStatus)
{
    // local block and global thread indices
    int x = threadIdx.x;
    int gx = x + blockIdx.x*blockDim.x;
    
    // index of the gamma random variable this thread samples
    int r = gx/M;
    
    // this is the m-th of the M threads responsible for r
    int m = gx-r*M;
    
    // Initialize shared memory across block
    // Each thread samples a value from the proposal dist
    __shared__ float gamma[B];
    gamma[x] = 0.0;
    
    // Store whether or not thread's value was accepted
    __shared__ int accept[B];
    accept[x] = 0;
        
    // Only run if this thread corresponds to a valid sample
    if (r<R)
    {
        // get the random variates
        float u = pU[gx];
        float n = pN[gx];
        
        // get  the gamma params
        float a = pAlpha[r];
    
        // The Marsaglia algorithm requires extra effort for a<1.0
        // which we have not implemented.
        if (a < 1.0)
        {
            if (m==0)
            {
                pStatus[r] = ERROR_INVALID_PARAMETER;
            }
            return;
        }
        
        float d = a-1.0/3.0;
        
        // the square root contributes significantly to each thread's run time
        float c = 1.0/sqrtf(9.0*d);
                
//        float v = powf(1+c*n,3);
        float ocn = 1+c*n;
        float v = ocn*ocn*ocn;
    
        // if v <= 0 this result is invalid
        if (v>0)
        {
//            float n2 = n*n;
            if (u <=(1-0.0331*(n*n)*(n*n)))
            {
                // rejection sample. The second operation should be
                // performed with low probability. This is the "squeeze"
                gamma[x] = d*v;
                accept[x] = true;
            }
            if (logf(u)<0.5*(n*n)+d*(1-v+logf(v)))
            {
                // rejection sample. The second operation should be
                // performed with low probability. This is the "squeeze"
                gamma[x] = d*v;
                accept[x] = true;
            }
        }
    
    }
    

    // Reduce across block to find the first accepted sample
    __syncthreads();
    for (int half=M/2; half>0; half>>=1)
    {
        __syncthreads();
       if(m < half && r<R)
       {
           // if the latter sample was accepted but the current
           // was not, copy the latter to the current. If the current
           // was accepted we keep it. If neither was accepted we
           // don't change anything.
           if (!accept[x])
           {
               if (accept[x+half])
               {

                   gamma[x] = gamma[x+half];
                   accept[x] = 1;

               }
           }
       }

    }
    __syncthreads();
    
    // Store the sample to global memory, or return error if failure
    if (m==0 && r<R)
    {
        if (accept[x])
        {
            // rescale the result (assume rate characterization)
            float b = pBeta[r];
            pG[r] = gamma[x]/b;
            pStatus[r] = ERROR_SUCCESS;
        }
        else
        {
            pStatus[r] = ERROR_SAMPLE_FAILURE;
        }
    }
    
}

__global__ void gamrnd_acc(int R,
                       float* pU,
                       float* pN,
                       float* pAlpha,
                       float* pBeta,
                       float* pGall,
                       int* pAccept)
{
    // local block and global thread indices
    int x = threadIdx.x;
    int gx = x + blockIdx.x*blockDim.x;
    
    // index of the gamma random variable this thread samples
    int r = gx/M;
    
    // Initialize shared memory across block
    // Each thread samples a value from the proposal dist
    __shared__ float gamma[B];
    gamma[x] = 0.0;
    
    // Store whether or not thread's value was accepted
    __shared__ int accept[B];
    accept[x] = 0;
        
    // Only run if this thread corresponds to a valid sample
    if (r<R)
    {
        // get the random variates
        float u = pU[gx];
        float n = pN[gx];
        
        // get  the gamma params
        float a = pAlpha[r];
        float b = pBeta[r];
    
//        // The Marsaglia algorithm requires extra effort for a<1.0
//        // which we have not implemented.
//        if (a < 1.0)
//        {
//            if (m==0)
//            {
//                pStatus[r] = ERROR_INVALID_PARAMETER;
//            }
//            return;
//        }
//        
        float d = a-1.0/3.0;
        
        // the square root contributes significantly to each thread's run time
        float c = 1.0/sqrtf(9.0*d);
                
//        float v = powf(1+c*n,3);
        float ocn = 1+c*n;
        float v = ocn*ocn*ocn;
    
        // if v <= 0 this result is invalid
        if (v>0)
        {
//            float n2 = n*n;
            if (u <=(1-0.0331*(n*n)*(n*n)))
            {
                // rejection sample. The second operation should be
                // performed with low probability. This is the "squeeze"
                gamma[x] = d*v;
                accept[x] = 1;
            }
            else if (logf(u)<0.5*(n*n)+d*(1-v+logf(v)))
            {
                // rejection sample. The second operation should be
                // performed with low probability. This is the "squeeze"
                gamma[x] = d*v;
                accept[x] = 1;
            }
        }
        
        pAccept[gx] = accept[x];
        
        // rescale the result (assume rate characterization)
        pGall[gx] = gamma[x]/b;
    }    
    
    
}

__global__ void gamrnd_red(int R,
                           float* pGamma,
                           int* pAccept,
                           float* pG,
                           int* pStatus)
{
    // local block and global thread indices
    int x = threadIdx.x;
    int gx = x + blockIdx.x*blockDim.x;
    
    // index of the gamma random variable this thread samples
    int r = gx/M;
    
    // this is the m-th of the M threads responsible for r
    int m = gx-r*M;
    
    // store gammas
    __shared__ float gamma[B];
    gamma[x] = 0.0;
    
    // Store whether or not thread's value was accepted
    __shared__ int accept[B];
    accept[x] = 0;

    // test reduction
    if (r<R)
    {
        gamma[x] = pGamma[gx];
        accept[x] = pAccept[gx];
    }

    __syncthreads();
    for (int half=M/2; half>0; half>>=1)
    {
        __syncthreads();
       if(m < half && r<R)
       {
           if (!accept[x])
             {
                 if (accept[x+half])
                 {
                     accept[x] = 1;
                     gamma[x] = gamma[x+half];
                 }
             }
       }
    }
            
    __syncthreads();
    // Store the sample to global memory, or return error if failure
    if (m==0 && r<R)
    {
        if (accept[x])
        {
            pG[r] = gamma[x];
            pStatus[r] = ERROR_SUCCESS;
        }
        else
        {
            pStatus[r] = ERROR_SAMPLE_FAILURE;
//            bool any_accepted = false;
//            for (int j=0; j<M; j++)
//            {
//                any_accepted |= accept[x+j];
//            }
//            if (any_accepted)
//            {
//                pStatus[r] = ERROR_BUG;
//            }
//            else
//            {
//                pStatus[r] = ERROR_SAMPLE_FAILURE;
//            }
        }
    }
}